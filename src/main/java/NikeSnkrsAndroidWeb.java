import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.WaitOptions;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;

import static io.appium.java_client.touch.offset.PointOption.point;

public class NikeSnkrsAndroidWeb {
    WebDriver driver;
    String chromeDriverPath = System.getProperty("user.dir") +"\\driver\\chromedriver.exe";
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;
    String baseURL = "https://www.nike.com/launch";
    String productURL = "https://www.nike.com/launch/t/womens-blazer-low-platform-white";


    public NikeSnkrsAndroidWeb() throws IOException {
    }

    @BeforeTest
    public void browserUpAndRun() throws InterruptedException, IOException {
        System.out.println("*******Starting the Appium server********");
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        System.out.println("Appium Service Address: "+ appiumServiceUrl);
        appiumService.clearOutPutStreams();

        Thread.sleep(2000);
        Runtime.getRuntime().exec("adb connect localhost:21503");
        System.out.println("MEmu adb connected");

        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("deviceVersion", "7.1.2");
        dc.setCapability("browserName", "Chrome");
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        driver.get(baseURL);
    }

    //int numberOfAds = (ReadExcelNumber.readData(excelPath, "0", 3, "3"));
    private Object[][] data = new Object[0][0];
    @DataProvider(name="NumberProvider")
    public Object[][] getNumber() {
        data = new Object[1][1];
        return data;
    }

    @Test(dataProvider="NumberProvider")
    public void buySnkrs(String args) throws Exception {
        By addToCart = By.xpath("//button[contains(text(),'add to cart')]");
        By size = By.xpath("//button[contains(text(),'W 7 / M 5.5')]");
        By loginBtn = By.xpath("//header/div[1]/section[1]/div[1]/ul[1]/li[1]/button[1]");
        By emailAddressField = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[2]/input[1]");
        By passwordField = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[3]/input[1]");

        By signInBtn = By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/form[1]/div[6]/input[1]");
        waitForClickAbilityOf(loginBtn);
        driver.findElement(loginBtn).click();

        String loginID = "dom.maybe@gmail.com";
        String password = "Dunker42";

        waitForVisibilityOf(emailAddressField);
        driver.findElement(emailAddressField).sendKeys(loginID);
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(signInBtn).click();


        WebElement element = driver.findElement(addToCart);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);

        driver.findElement(size).click();
        //Thread.sleep(10000);
        driver.findElement(addToCart).click();

    }


    @AfterTest
    public void endBot() throws IOException {
        //driver.quit();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(point(anchor, endPoint)).release().perform();
    }

    protected void waitForVisibilityLong(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 300);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForClickAbilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
}
