import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.WaitOptions;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;

import static io.appium.java_client.touch.offset.PointOption.point;

public class NikeSnkrsAndroid {
    WebDriver driver;
    String excelPath = System.getProperty("user.dir") +"\\excel_file\\Input.xlsx";
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;

    public NikeSnkrsAndroid() throws IOException {
    }

    @BeforeTest
    public void runAppiumServer() throws InterruptedException, IOException {
        System.out.println("*******Starting the Appium server********");
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        System.out.println("Appium Service Address: "+ appiumServiceUrl);
        appiumService.clearOutPutStreams();

        Thread.sleep(2000);
        Runtime.getRuntime().exec("adb connect localhost:21503");
        System.out.println("MEmu adb connected");


        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("noReset", "true");
        dc.setCapability("autoGrantPermissions", "true");
        dc.setCapability("appPackage", "com.nike.snkrs");
        dc.setCapability("appActivity", "com.nike.snkrs.feed.activities.TheWallActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);

    }

    int numberOfProduct = (ReadExcelNumber.readData(excelPath, "0", 3, "2"));
    private Object[][] data = new Object[0][0];
    @DataProvider(name="NumberProvider")
    public Object[][] getNumber() {
        data = new Object[numberOfProduct][1];
        return data;
    }

    @Test(dataProvider="NumberProvider")
    public void buySnkrs(String args) throws Exception {
        By feedTab = By.id("com.nike.snkrs:id/feedTab");
        By inStockTab = By.id("com.nike.snkrs:id/droppedTab");
        By productView = By.id("com.nike.snkrs:id/imageCardImage");
        By priceBtn = By.id("com.nike.snkrs:id/threadCtaButton");
        By selectTheItem = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.view.ViewGroup[2]/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.ImageView");
        By searchBtn = By.id("com.nike.snkrs:id/searchNavButton");
        By searchField = By.id("com.nike.snkrs:id/droppedTab");
        By searchField2 = By.id("com.nike.snkrs:id/searchEditTextView");
        By searchResult = By.id("com.nike.snkrs:id/item_discover_search_entire_row");
        String sizeValue = ReadExcel.readData(excelPath, "0", 6, String.valueOf(2));
        By size = By.xpath("//android.widget.TextView[@text= '"+sizeValue+"']");
        By okBtn = By.id("com.nike.snkrs:id/fragment_shoe_size_ok_button");
        By shippingBtn = By.id("com.nike.snkrs:id/fragment_prereceipt_shipping_row");
        By firstNameField = By.id("com.nike.snkrs:id/view_address_entry_first_name_edittext");
        By lastNameField = By.id("com.nike.snkrs:id/view_address_entry_last_name_edittext");
        By addressField = By.id("com.nike.snkrs:id/view_address_entry_line1_edittext");
        By aptField = By.id("com.nike.snkrs:id/view_address_entry_line2_edittext");
        By zipField = By.id("com.nike.snkrs:id/view_address_entry_postal_code_edittext");
        By cityField = By.id("com.nike.snkrs:id/view_address_entry_city_edittext");
        By phoneField = By.id("com.nike.snkrs:id/view_address_entry_phone_edittext");
        By paymentBtn = By.id("com.nike.snkrs:id/fragment_prereceipt_payment_row");
        By payPalBtn = By.id("com.nike.snkrs:id/fragment_payment_options_add_paypal_button");

        waitForClickAbilityOf(inStockTab);
        driver.findElement(inStockTab).click();

        waitForClickAbilityOf(searchBtn);
        driver.findElement(searchBtn).click();

        String productSKU = ReadExcel.readData(excelPath, "0", 6, String.valueOf(1));
        driver.findElement(searchField).click();
        driver.findElement(searchField2).sendKeys(productSKU);

        waitForClickAbilityOf(searchResult);
        driver.findElement(searchResult).click();

       /* swipeVertical((AppiumDriver)driver,0.9,0.1,0.5,3000);
        swipeVertical((AppiumDriver)driver,0.9,0.1,0.5,3000);*/

        waitForClickAbilityOf(priceBtn);
        driver.findElement(priceBtn).click();

        waitForClickAbilityOf(size);
        driver.findElement(size).click();

        waitForClickAbilityOf(okBtn);
        driver.findElement(okBtn).click();

        waitForClickAbilityOf(paymentBtn);
        driver.findElement(paymentBtn).click();

        waitForClickAbilityOf(payPalBtn);
        driver.findElement(payPalBtn).click();

        Thread.sleep(30000);
    }


    @AfterTest
    public void endBot() throws IOException {
        driver.quit();
        System.out.println("Stop Appium service");
        appiumService.stop();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void swipeVertical(AppiumDriver driver, double startPercentage, double finalPercentage, double anchorPercentage, int duration) throws Exception {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * finalPercentage);
        new TouchAction(driver).press(point(anchor, startPoint)).waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration))).moveTo(point(anchor, endPoint)).release().perform();
    }

    protected void waitForVisibilityLong(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 300);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForClickAbilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
}
